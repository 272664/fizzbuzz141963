import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {

    @Test
    void fizzBuzz() {
        String str = FizzBuzz.fizzBuzz(15);
        assertEquals("12Fizz4BuzzFizz78FizzBuzz11Fizz1314Fizz_Buzz", str);
    }
}