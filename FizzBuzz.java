public class FizzBuzz {
    public static String fizzBuzz(int n){
        String str = "";

        for (int i=1;i<=n;i++){
            if (i%15 == 0){
                str = str + "Fizz_Buzz";
            }else if (i%3 == 0){
                str = str + "Fizz";
            } else if (i%5 == 0) {
                str = str + "Buzz";
            } else{
                str = str + String.valueOf(i);
            }
        }

        return str;
    }
}
